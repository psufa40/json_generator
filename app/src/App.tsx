import React, { Fragment, useState, useMemo } from 'react';
import { JsonForms } from '@jsonforms/react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
// import logo from './logo.svg';
import './App.css';
import schema from './schema.json';
import uischema from './uischema.json';
import {
  materialCells,
  materialRenderers,
} from '@jsonforms/material-renderers';
import RatingControl from './RatingControl';
import ratingControlTester from './ratingControlTester';
import { makeStyles } from '@mui/styles';
// import { saveAs } from 'file-saver';
import stripJsonComments from 'strip-json-comments';
import { autocompleteClasses } from '@mui/material';
// import Highlighter from "react-highlight-words";
// import { updatedDiff } from 'deep-object-diff';
// import { convertCompilerOptionsFromJson } from 'typescript';

const useStyles = makeStyles({
  container: {
    padding: '1em',
    width: '100%',
  },
  title: {
    textAlign: 'center',
    padding: '0.25em',
  },
  dataContent: {
    display: 'flex',
    justifyContent: 'left',
    borderRadius: '1.25em',
    backgroundColor: '#cecece',
    fontSize: '80%',
    marginBottom: '1rem',
  },
  // dataContent2: {
  //   // whiteSpace: 'pre-line',
  //   marginLeft: '20px !important',
  //   wordBreak: 'break-word',
  //   // display: 'flex',
  //   // boxSizing: 'border-box',
  //   width: 'auto',
  //   maxWidth: '100%',
  //   overflow: 'auto',
  //   // flexWrap: 'wrap',
  //   hyphens: 'auto',
  // },
  yellow_bg:{
    backgroundColor: "yellow",
  },
  dataContent3: {
    whiteSpace: 'pre',
    marginTop: "13px",
    marginLeft: '20px !important',
    wordBreak: 'break-word',
    // display: 'flex',
    boxSizing: 'border-box',
    width: 'auto',
    maxWidth: '100%',
    overflow: 'auto',
    height: '1250px',
    // weith: '1650px',
    // flexWrap: 'wrap',
    hyphens: 'auto',
    overflowY: 'scroll',
  },
  resetButton: {
    // padding: '10rem  !important',
    marginLeft: '180px !important',
    // marginRight: '100px !important',
    // position: 'relative !important',
    // marginleft: '2 !important',
    // display: 'block !important',
  },
  demoform: {
    margin: 'auto',
    padding: '1rem',
  },
});

const renderers = [
  ...materialRenderers,
  //register custom renderers
  { tester: ratingControlTester, renderer: RatingControl },
];



const initialData = {
  Phases: [
    {
      "PhaseName": "Фаза 1",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "Start",
          "IP": "127.0.0.1",
          "User": "test",
          "PwdEncrypted": "01000000d08c00acd65f1917941546a4f27932c3adfcb70000000002000000000003660000c00000001000000017e4f579776674aad8f485bcd997eb110000000004800000a000000010000000b7c48a41c013ca8e5683d7559ddf01",
          "Services": [
            {
              "ServiceName": "W3C",
              "Comment": "IIS",
              "Ports": [
                80,
                443
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\test*.log"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            }
          ],
          "Comment": "222"
        },
        {
          "ServerName": "Stop",
          "IP": "127.0.0.2",
          "User": "Admin",
          "PwdEncrypted": "Mypassword",
          "Services": [
            {
              "ServiceName": "W3SVC",
              "Comment": "IIS",
              "Ports": [
                80,
                443
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\test*.log"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            }
          ],
          "Comment": "222"
        }
      ]
    },
    {
      "PhaseName": "Фаза 2",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "next",
          "IP": "127.0.0.1",
          "User": "test\\test",
          "PwdEncrypted": "0100000",
          "Services": [
            {
              "ServiceName": "tester",
              "Comment": "",
              "Ports": [],
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log",
                  "LoggingLevelConfig": {
                    "XML": "C:\\test.config",
                    "XPath": "/configuration/log4net/root/level",
                    "RequiredLevel": "DEBUG"
                  },
                  "SuccessfulStartString": ""
                }
              ],
              "RequiredServices": [
                "ForisSecurity"
              ],
              "RequiredPorts": []
            }
          ]
        }
      ]
    },
    {
      "PhaseName": "Фаза 4",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "IIS",
          "Comment": "Test Comment",
          "IP": "192.178.9.78",
          "User": "54hgdgshe",
          "PwdEncrypted": "01000000d08c9d",
          "Services": [
            {
              "ServiceName": "sddfsf",
              "RequiredServices": [],
              "DelayAfterStartSec": 15,
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log"
                }
              ]
            },
            {
              "ServiceName": "fsdfsdf",
              "RequiredServices": [
                "PowerS22"
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\tesy52.log"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};



const App = () => {
  const classes = useStyles();
  const [jsonData, setJsonData] = useState<any>(initialData);
  const [viewData, setViewData] = useState<any>(initialData);

  const stringifiedData = useMemo(
    () => JSON.stringify(viewData?.Phases, null, 2),
    [viewData]
  );

  const exportFile = () => {
    const element = document.createElement('a');
    var jsonse = JSON.stringify(jsonData?.Phases, null, 2);
    const textFile = new Blob([jsonse], { type: 'application/json' });
    element.href = URL.createObjectURL(textFile);
    element.download = 'userFile.json';
    document.body.appendChild(element);
    element.click();
  };

  const clearData = () => {
    window.location.reload();
  };
 

  const onChangeFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        const target = e.target;
        const result = target?.result;
        let userObj =
          typeof fileReader.result == 'string'
            ? JSON.parse(stripJsonComments(fileReader.result))
            : {};
        const data2 = { Phases: userObj };
        setJsonData(data2);
        setViewData(data2);

      };
      fileReader.readAsText(e.target.files[0]);
    }
  };

  function compared(prevData: any, newData: any){
    if (!Array.isArray(newData)) return null
    let comparedData: object[] = []
    newData.forEach((phase, i) => {
      if (typeof newData !== "object") return null
      comparedData[i] = compareData(prevData[i], phase)
    })

    return {Phases: comparedData}
  }

  function compareData(prevData: any, newData: any, isBacklight?: boolean) {
    if (typeof newData === "string" ||
        typeof newData === "number" ||
        typeof newData === "boolean"
    ){
      if(prevData !== newData || isBacklight){
        return (`<span style=background-color:yellow>${newData}</span>`)
      }

      return newData
    }

    if (Array.isArray(newData)) {
      let newArr: object[] = []
      newData.forEach((item, i) => {
        if(!prevData || !prevData[i]) {
          newArr[i] = compareData(newData[i], item, true)
        } else {
          newArr[i] = compareData(prevData[i], item)
        }
      })
      return newArr
    }
    if (typeof newData === "object"){
      let newObj: any = {}
      for (let key in newData){
        if(!prevData || !prevData[key]){
          newObj[key] = compareData(newData[key], newData[key], true)
        }else {
          newObj[key] = compareData(prevData[key], newData[key])
        }
      }
      return newObj
    }
  }

  return (
    <Fragment>
      <div className='App'>
        <header className='App-header'>
          {/* <img src={logo} className='App-logo' alt='logo' /> */}
          <h1 className='App-title'>Welcome to Rendereing Json</h1>
          <p className='App-intro'></p>
        </header>
      </div>
      <Grid
        container
        justifyContent={'center'}
        spacing={1}
        className={classes.container}
      >
        <Grid item sm={6}>
          <Button
            className={classes.resetButton}
            onClick={clearData}
            color='primary'
            variant='contained'
          >
            Clear data
          </Button>

          <Button
            className={classes.resetButton}
            onClick={() => exportFile()}
            color='primary'
            variant='contained'
          >
            DownLoad
          </Button>
          <Button
            variant='contained'
            className={classes.resetButton}
            component='label'
          >
            Upload File
            <input type='file' id='input_json' onChange={onChangeFile} hidden />
          </Button>
          <Typography variant={'h4'} className={classes.title}>
            Bound data
          </Typography>

          <div className={classes.dataContent}>
            <div className={classes.dataContent3} dangerouslySetInnerHTML={{__html: stringifiedData}}></div>
          </div>
        </Grid>

        <Grid item sm={6}>
          <Typography variant={'h4'} className={classes.title}>
            Rendered form
          </Typography>
          <div className={classes.demoform}>
            <JsonForms
              schema={schema}
              uischema={uischema}
              data={jsonData}
              renderers={renderers}
              cells={materialCells}
              onChange={({ errors, data }) => {
                const dataForCompare = JSON.parse(JSON.stringify(data))
                const dataWithBgColor = compared(jsonData.Phases, dataForCompare.Phases)
                const dataComparedWithOldView = compared(viewData.Phases, dataWithBgColor?.Phases)
                setViewData(dataComparedWithOldView)
                setJsonData(data)
              }}
            />
          </div>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default App;


