import React, { Fragment, useState, useMemo } from 'react';
import { JsonForms } from '@jsonforms/react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
// import logo from './logo.svg';
import './App.css';
import schema from './schema.json';
import uischema from './uischema.json';
import {
  materialCells,
  materialRenderers,
} from '@jsonforms/material-renderers';
import RatingControl from './RatingControl';
import ratingControlTester from './ratingControlTester';
import { makeStyles } from '@mui/styles';
// import { saveAs } from 'file-saver';
import stripJsonComments from 'strip-json-comments';
import Highlighter from "react-highlight-words";
import { updatedDiff } from 'deep-object-diff';
import { convertCompilerOptionsFromJson } from 'typescript';

const useStyles = makeStyles({
  container: {
    padding: '1em',
    width: '100%',
  },
  title: {
    textAlign: 'center',
    padding: '0.25em',
  },
  dataContent: {
    display: 'flex',
    justifyContent: 'left',
    borderRadius: '1.25em',
    backgroundColor: '#cecece',
    fontSize: '80%',
    marginBottom: '1rem',
  },
  dataContent2: {
    // whiteSpace: 'pre-line',
    marginLeft: '20px !important',
    wordBreak: 'break-word',
    // display: 'flex',
    // boxSizing: 'border-box',
    width: 'auto',
    maxWidth: '100%',
    overflow: 'auto',
    // flexWrap: 'wrap',
    hyphens: 'auto',
  },
  resetButton: {
    // padding: '10rem  !important',
    marginLeft: '180px !important',
    // marginRight: '100px !important',
    // position: 'relative !important',
    // marginleft: '2 !important',
    // display: 'block !important',
  },
  demoform: {
    margin: 'auto',
    padding: '1rem',
  },
});

const renderers = [
  ...materialRenderers,
  //register custom renderers
  { tester: ratingControlTester, renderer: RatingControl },
];

const initialData3 = {
  Phases: [
    {
      PhaseName: '11',
      Comment: '12',
      Servers: [
        {
          ServerName: '13',
          Comment: '14',
          IP: '15',
          User: '16',
          PwdEncrypted:
            '17',
        },
      ],
    },
    {
      PhaseName: '21',
      Comment: '22',
      Servers: [
        {
          ServerName: '23',
          Comment: '24',
          IP: '25',
          User: '26',
          PwdEncrypted: '27',
        },
      ],
    },
  ],
};

const initialData = {
  Phases: [
    {
      "PhaseName": "Фаза 1",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "22",
          "IP": "444",
          "User": "test",
          "PwdEncrypted": "01000000d08c9ddf01",
          "Services": [
            {
              "ServiceName": "W3C",
              "Comment": "IIS",
              "Ports": [
                80,
                443
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\test*.log"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            },
            {
              "ServiceName": "W3C1",
              "Comment": "IIS1",
              "Ports": [
                80,
                443
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\test*.log1"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            }
          ],
          "Comment": "222"
        }
      ]
    },
    {
      "PhaseName": "Фаза 2",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "next",
          "IP": "127.0.0.1",
          "User": "test\\test",
          "PwdEncrypted": "0100000",
          "Services": [
            {
              "ServiceName": "tester",
              "Comment": "",
              "Ports": [],
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log",
                  "LoggingLevelConfig": {
                    "XML": "C:\\test.config",
                    "XPath": "/configuration/log4net/root/level",
                    "RequiredLevel": "DEBUG"
                  },
                  "SuccessfulStartString": ""
                }
              ],
              "RequiredServices": [
                "ForisSecurity"
              ],
              "RequiredPorts": []
            }
          ]
        }
      ]
    },
    {
      "PhaseName": "tete",
      "Comment": "test545",
      "Servers": [
        {
          "ServerName": "45gf",
          "Comment": "54yg",
          "IP": "45tgfd",
          "User": "54hgdgshe",
          "PwdEncrypted": "01000000d08c9d",
          "Services": [
            {
              "ServiceName": "sddfsf",
              "RequiredServices": [],
              "DelayAfterStartSec": 15,
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log"
                }
              ]
            },
            {
              "ServiceName": "fsdfsdf",
              "RequiredServices": [
                "PowerS22"
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\tesy52.log"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};

const initialData2 = {
  Phases: [
    {
      "PhaseName": "Фаfdsfsза 1",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "2gdfgdf2",
          "IP": "44gdfg4",
          "User": "tefdgdfst",
          "PwdEncrypted": "010000fdgdf08c9ddf01",
          "Services": [
            {
              "ServiceName": "Wgfdg3C",
              "Comment": "IIfdggfdS",
              "Ports": [
                80,
                443
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\tefdffd*.log"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            },
            {
              "ServiceName": "W3121",
              "Comment": "II212S",
              "Ports": [
                82120,
                442123
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\t45435345*.log212"
                }
              ],
              "RequiredServices": [],
              "RequiredPorts": []
            }
          ],
          "Comment": "222"
        }
      ]
    },
    {
      "PhaseName": "Фаза 2",
      "Comment": "",
      "Servers": [
        {
          "ServerName": "next",
          "IP": "127.0.0.1",
          "User": "test\\test",
          "PwdEncrypted": "0100000",
          "Services": [
            {
              "ServiceName": "tester",
              "Comment": "",
              "Ports": [],
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log",
                  "LoggingLevelConfig": {
                    "XML": "C:\\test.cfdsfig",
                    "XPath": "/conffsdfnet/root/level",
                    "RequiredLevel": "DEBsdf"
                  },
                  "SuccessfulStartString": ""
                }
              ],
              "RequiredServices": [
                "ForisSecurity"
              ],
              "RequiredPorts": []
            }
          ]
        }
      ]
    },
    {
      "PhaseName": "tete",
      "Comment": "test545",
      "Servers": [
        {
          "ServerName": "45gf",
          "Comment": "54yg",
          "IP": "45tgfd",
          "User": "54hgdgshe",
          "PwdEncrypted": "01000000d08c9d",
          "Services": [
            {
              "ServiceName": "sddfsf",
              "RequiredServices": [],
              "DelayAfterStartSec": 15,
              "LogFiles": [
                {
                  "FilePath": "C:\\test.log"
                }
              ]
            },
            {
              "ServiceName": "fsdfsdf",
              "RequiredServices": [
                "PowerS22"
              ],
              "LogFiles": [
                {
                  "FilePath": "C:\\tesy52.log"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};

const App = () => {
  const classes = useStyles();
  const [data, setData] = useState<any>(initialData);
  const [data3, setData3] = useState<any>(initialData);
  const stringifiedData = useMemo(
    () => JSON.stringify(data?.Phases, null, 2),
    [data]
  );

  const exportFile = () => {
    const element = document.createElement('a');
    var jsonse = JSON.stringify(data?.Phases, null, 2);
    const textFile = new Blob([jsonse], { type: 'application/json' });
    element.href = URL.createObjectURL(textFile);
    element.download = 'userFile.json';
    document.body.appendChild(element);
    element.click();
  };

  const clearData = () => {
    window.location.reload();
  };


  const dd = [""]
  const [tt, setData2] = useState<any>(dd);
  // const [myData, setMyData] = useState<any>([]);
  const [names, setNames] = useState(['5']);
  // const [shopCart, setShopCart] = useState({ });
  // var rows = [];
  const [todos, setTodos] = useState(['']);
  const newTodos = todos.slice();
  const difffunc = () => {
    const a1 = updatedDiff(data3, data)
    console.log('main diffrent', a1)
    const a2 = Object.values(a1)[0]
    const a3 =        // Count Phases
      a2 !== undefined
        ? Object.values(a2).length
        : 0
    for (let i = 0; i < a3+1; i++) { // Loop Phases
      const b1 = a2[i]
      const b2 = 
        b1 !== undefined
          ? Object.values(b1)[0]
          : ''
      newTodos.push(b2 as string);
      const b3 =         // Array Servers with checking not null
        b1?.Servers !== undefined 
          ? Object.values(b1?.Servers)
          : 0;
      const b4 = 
        b3 !== undefined
        ? Object.values(b3).length // Count Servers element
        : 0
      for (let i = 0; i < b4; i++) { // Loop Servers
        const c1 = // Values from Servers with checking not null
          b3 == 0
            ? ''
            : b3[i];
                          //////////////+
        const c2 = JSON.stringify(c1) // this is stupid code
        const c3 = // this is stupid code
          typeof c2 == 'string'
            ? JSON.parse(c2)
            : {};
                      //////////////+
        const c4 = Object.values(c3) // Values from arrat Server
        const d1 =                      // Count Services
          c3?.Services !== undefined 
            ? Object.values(c3?.Services).length
            : '';
        for (let i = 0; i < d1; i++) { // Loop Services
          const d2 = Object.values(c3?.Services)[i] //
                  ///////////////////-
          const d3 = JSON.stringify(d2)
          const d4= // this is stupid code
            typeof d3 == 'string'
              ? JSON.parse(d3)
              : {};
                    /////////////////-
          const d5 = Object.values(d4) // Values from Services
          const d6 = d5.toString() // Add value in Main Array
          newTodos.push(d6); // Add value in Main Array (services)
          const e1 = d4?.Ports // Array from Ports
          const e2 =  // Length array ports
            e1 !== undefined 
              ? Object.values(e1).length
              : '';
          for (let i = 0; i < e2; i++) {  // Loop Ports
            const e3 = Object.values(e1)[i]
            const e4= // this is stupid code
              typeof e3== 'number'
                ? e3.toString()
                : '';
            // const f2 = f1.toString() // value to string
            newTodos.push(e4); // Push value in main array (ports)
          }

          const f1 = 
            d4?.LogFiles !== undefined 
            ? Object.values(d4?.LogFiles[0]) // Array from LogFiles
            : '';
          const f2 = f1.toString()
          newTodos.push(f2); // Add value in Main Array
          const f3 = // Array from SuccessfulStartString
            d4?.SuccessfulStartString !== undefined 
              ? Object.values(d4?.SuccessfulStartString[0])
              : '';
          const f4 = f3.toString() // // Change Value to string fot add in Main Array
          newTodos.push(f4); // Add value in Main Array
          const g1 = 
            d4?.LogFiles !== undefined 
              ? Object.values(d4?.LogFiles) 
              : '';
          const g2 = g1[0]
            ////////////////
          const g3 = JSON.stringify(g2) // this is stupid code
          const g4= // this is stupid code
            typeof g3 == 'string'
              ? JSON.parse(g3)
              : {};
            /////////////////////
          const g5 = 
            g4?.LoggingLevelConfig !== undefined 
              ? Object.values(g4?.LoggingLevelConfig)
              : 0;
          const g6 = g5.toString() // Change Value to string fot add in Main Array 
          newTodos.push(g6); // Add value in Main Array

          // for (let i = 0; i < f2; i++) {  // Loop Ports
          //   const f3 = Object.values(f1)[i]
          //   const f4= // this is stupid code
          //     typeof f3 == 'number'
          //       ? f3.toString()
          //       : '';
          //   // const f2 = f1.toString() // value to string
          //   newTodos.push(f4);
          //   console.log(newTodos)
          // }

              // const demo3 = JSON.stringify(demo2)
          // newTodos.push(demo3);
          // console.log(newTodos)
          // console.log(demo2)
        }
        const c5 = c4.toString() // value to string
        newTodos.push(c5); // add value in new array
      }
      const t13 = newTodos.toString()
      var t14 = t13.split(',');
      const result = t14.filter(word => word.length > 1);
      console.log(result)
      setData2(result);
    }
  };



  const onChangeFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        const target = e.target;
        const result = target?.result;
        let userObj =
          typeof fileReader.result == 'string'
            ? JSON.parse(stripJsonComments(fileReader.result))
            : {};
        const data2 = { Phases: userObj };
        setData(data2);
        setData3(data2);
      };
      fileReader.readAsText(e.target.files[0]);
    }
  };



  return (
    <Fragment>
      <div className='App'>
        <header className='App-header'>
          {/* <img src={logo} className='App-logo' alt='logo' /> */}
          <h1 className='App-title'>Welcome to Rendering Json</h1>
          <p className='App-intro'></p>
        </header>
      </div>
      <Grid
        container
        justifyContent={'center'}
        spacing={1}
        className={classes.container}
      >
        <Grid item sm={6}>
          <Button
            className={classes.resetButton}
            onClick={clearData}
            color='primary'
            variant='contained'
          >
            Clear data
          </Button>
          <Button
            className={classes.resetButton}
            onClick={() => difffunc()}
            color='primary'
            variant='contained'
          >
            test
          </Button>
          <Button
            className={classes.resetButton}
            onClick={() => exportFile()}
            color='primary'
            variant='contained'
          >
            DownLoad
          </Button>
          <Button
            variant='contained'
            className={classes.resetButton}
            component='label'
          >
            Upload File
            <input type='file' id='input_json' onChange={onChangeFile} hidden />
          </Button>
          <Typography variant={'h4'} className={classes.title}>
            Bound data
          </Typography>
          {/* <Highlighter     highlightClassName="YourHighlightClass"
            searchWords={["55", "or", "the"]}
            autoEscape={true}
            textToHighlight={stringifiedData}></Highlighter> */}

          <div className={classes.dataContent}>
            <pre id='boundData' className={classes.dataContent2}>
            <Highlighter highlightClassName="YourHighlightClass"
            searchWords={tt}
            autoEscape={true}
            textToHighlight={stringifiedData}>
            </Highlighter>
              {/* {stringifiedData} */}
            </pre>
          </div>
        </Grid>
        <Grid item sm={6}>
          <Typography variant={'h4'} className={classes.title}>
            Rendered form
          </Typography>
          <div className={classes.demoform}>
            <JsonForms
              schema={schema}
              uischema={uischema}
              data={data}
              renderers={renderers}
              cells={materialCells}
              onChange={({ errors, data }) => setData(data)}
            />
          </div>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default App;
